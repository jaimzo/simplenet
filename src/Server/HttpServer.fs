namespace SimpleNet.Server

// Simple HTTP server to supply a network interface to
// the library. There are, needless to say, much better ways of 
// doing this...

// From http://fssnip.net/pj

open System.IO
open System.Net
open System.Threading
open System.Text

[<AutoOpen>]
module HttpExtensions =
    type System.Net.HttpListener with
        member x.AsyncGetContext() =
            Async.FromBeginEnd(x.BeginGetContext, x.EndGetContext)

    type System.Net.HttpListenerRequest with
        member request.InputString =
            use sr = new StreamReader(request.InputStream)
            sr.ReadToEnd()
        
    type System.Net.HttpListenerResponse with
        member response.Reply(s: string) =
            let buffer = Encoding.UTF8.GetBytes(s)
            response.ContentLength64 <- int64 buffer.Length
            response.OutputStream.Write(buffer, 0, buffer.Length)
            response.OutputStream.Close()

        member response.Reply(typ, buffer:byte[]) =
            response.ContentLength64 <- int64 buffer.Length
            response.ContentType <- typ
            response.OutputStream.Write(buffer, 0, buffer.Length)
            response.OutputStream.Close()


type HttpServer private (url, root) =
    let contentTypes =
        dict [ ".css", "text/css"; ".html", "text/html"; ".js", "text/javascript"; 
               ".png", "image/png"; ".jpg", "image/jpg"; ".gif", "image/gif" ]

    let tokenSource = new CancellationTokenSource()

    let agent = MailboxProcessor<HttpListenerContext>.Start((fun inbox -> async {
        while true do
            let! context = inbox.Receive()
            try
                let route = context.Request.Url.AbsolutePath
                context.Response.Reply(route)
            with e ->
                context.Response.Reply(sprintf "Error processing request %A" e)}), tokenSource.Token)

    let server = async {
        use listener = new HttpListener()
        listener.Prefixes.Add(url)
        listener.Start()
        while true do
            let! context = listener.AsyncGetContext()
            agent.Post(context)
    }

    do Async.Start(server, cancellationToken = tokenSource.Token)

    member x.Stop() = tokenSource.Cancel()

    static member Start(url, root) =
        HttpServer(url, root)

type HttpAgent private (url, fn) as this =
    let tokenSource = new CancellationTokenSource()
    let agent = MailboxProcessor<HttpListenerContext>.Start((fun _ -> fn this), tokenSource.Token)
    let server = async {
        use listener = new HttpListener()
        listener.Prefixes.Add(url)
        listener.Start()
        while true do
            let! context = listener.AsyncGetContext()
            agent.Post(context)
    }
    do Async.Start(server, cancellationToken = tokenSource.Token)

    member x.Stop() = tokenSource.Cancel()

    static member Start(url, fn) =
        HttpAgent(url, fn)

