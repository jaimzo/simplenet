module SimpleNetServer

open System
open System.Threading

open Suave
open Suave.Filters
open Suave.Operators
open Suave.Successful
open Suave.RequestErrors

let dumpCtx (d) =
  context (fun ctx ->
    OK(ctx.ToString()))

let routes =
  choose
    [ GET >=> choose
        [ path "/hello" >=> OK "Hello GET" 
          path "/goodbye" >=> OK "Goodbye GET"
          pathScan "/ctx/%d" dumpCtx
          pathScan "/add/%d/%d" (fun (a, b) -> OK((a+b).ToString()))
          NOT_FOUND "Route not found" ]
      POST >=> choose
        [ path "/hello" >=> OK "Hello POST"
          path "/goodbye" >=> OK "Goodbye POST "]
      RequestErrors.NOT_FOUND "Route not found"]

let openServerOnConsole () =
  let cts = new CancellationTokenSource()
  let conf = { defaultConfig with cancellationToken = cts.Token }
  let listening, server = startWebServerAsync conf routes

  Async.Start(server, cts.Token)

  printfn "Make requests now"

  Console.ReadKey() |> ignore

  cts.Cancel()

  0