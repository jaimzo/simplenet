﻿namespace SimpleNet.Neural

module Net =

    open MathNet.Numerics
    open MathNet.Numerics.LinearAlgebra

    type OptimisationType =
        | None
        | Momentum
        | Adam


    type HyperParameters = {
        learningRate : float
        learningRateDecay : float

        momentumCoefficient  : float

        miniBatchSize : int
        normaliseBatches : bool

        layerCount : int
        hiddenUnitCount : int

        optimisation : OptimisationType

        regularisationCoefficient : float

        // Coefficient used for exponentially weighted averaging
        expWeightCoefficient : float

        adamBeta1 : float
        adamBeta2 : float

        epsilon : float

        dropProbability : float
    }


    type ActivationFn = 
        | Relu
        | Sigmoid
        | Tanh

    type Layer = { weights : Matrix<float>; actFn : ActivationFn }

    type Network = Layer list

    // TODO: Check what these are used for
    type NodesVec = { sv : Vector<float>; xv : Vector<float> }
    type NodesVecList = NodesVec list

    // TODO: Hyper-parameter
    let epochs = 1

    // TODO: Hyper-parameter
    let learningRate = 0.04


    let fnForActivationFn (aFn : ActivationFn) =
        match aFn with
        | Sigmoid -> SpecialFunctions.Logistic
        | Relu -> (fun (x : float) -> max 0.0 x)
        | Tanh -> tanh
    
    let derivativeForActivationFn (aFn : ActivationFn) =
        match aFn with
        | Relu -> (fun x -> if x > 0.0 then 1.0 else 0.0)
        | Sigmoid -> (fun x -> SpecialFunctions.Logistic(x) * (1.0 - SpecialFunctions.Logistic(x)))
        | Tanh -> (fun x -> 1.0 - (tanh(x) ** 2.0))


    /// Calculate the cross entropy cost given the (presumably final layer)
    /// activations A and the expected values Y
    let crossEntropyCost (A : Vector<float>) (Y : Vector<float>) =
        let m = Y.Count |> float
        let logProd = A.PointwiseLog().PointwiseMultiply(Y) + A.SubtractFrom(1.0).PointwiseLog().PointwiseMultiply(Y.SubtractFrom(1.0))
        - logProd.Sum() |> float |> (fun x -> x / m)