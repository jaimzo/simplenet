﻿// Learn more about F# at http://fsharp.org

open System
open SimpleNetServer
open SimpleNet.Neural.Net

open MathNet.Numerics.LinearAlgebra

let f (i) =
  i |> float


[<EntryPoint>]
let main argv =
  let vectorBuild = Vector<float>.Build

  let a = vectorBuild.Random(5, MathNet.Numerics.Distributions.ContinuousUniform())
  let y = vectorBuild.Random(5, MathNet.Numerics.Distributions.ContinuousUniform())

  printfn "%A" a
  printfn "%A" y
  let cost = crossEntropyCost a y
  printfn "Const: %f\n" cost
  0
  //SimpleNetServer.openServerOnConsole()